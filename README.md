# CloudsAssign2



## How to deploy?

Run all the commands in the shell script in this repository to create a scaleset with 2 instances, install dependencies, launch the web application for both the instances.

Once done, you can access the end point from the internet.

## Locust

Then run the locust command from your local machine to test the responsiveness of the server.
