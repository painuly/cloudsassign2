from urllib import request
from flask import Flask, request
import numpy as np

def abs_sin_int(lower, upper, N_array = [1, 10, 100, 1000, 10000, 100000, 1000000]): 
    integral = []
    for N in N_array:
        int_len = (upper - lower)/N
        centers = np.arange(lower+int_len/2, upper, int_len)
        values = np.abs(np.sin(centers))*int_len
        integral.append(np.sum(values))
    return integral


app = Flask(__name__)

@app.route("/")
def welcome():
    return "<h1>Hi!</h1> <p>Bienvenue to PRATEEK's numerical integral service!</p>"

# to be called as /integral?lower=0&upper=3.1415
@app.route("/integral")
def integral():
    lower = request.args.get('lower')
    upper = request.args.get('upper')
    lower = float(lower) if lower else None
    upper = float(upper) if upper else None
    if lower is None or upper is None or lower>upper:
        return "<h1>I don't feel good...</h1> \
            <p>The format used is not correct.<br> \
                Please use the following format: <p> \
            <code> /integral?lower=0&upper=3.1415 </code>"
    results = abs_sin_int(lower, upper)
    return '''<h1>Generated successfully</h1> \
        <p><em>Lower: </em> {} <br> \
            <em>Upper: </em> {} <br> \
            <em>results: </em> <b>{}</b></p>'''.format(lower, upper, results)

