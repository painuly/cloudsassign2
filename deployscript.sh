### Create Azure ScaleSet with 2 instances
az vmss create \
	--resource-group clouds2 \
	--name scalset \
	--image UbuntuLTS \
	--admin-username azureuser \
	--generate-ssh-keys \
	--instance-count 2 \
	--vm-sku Standard_B1ms \
	--upgrade-policy-mode automatic \
	--lb-sku standard \
	--zones 1 2 3


### Check the addresses and ports of the instances created
az vmss list-instance-connection-info --resource-group clouds2 --name scalset

### SSH into both the instances in seperate sessions and deploy the apps
ssh azureuser@4.246.157.150 -p 50002

### Install all the necessary linux tools
sudo apt update 
sudo apt-get install python python3-pip nginx

### Create virtual environment
pip3 install virtualenv	
python3 -m virtualenv flaskenv 	

### Install dependencies to python environment
source flaskenv/bin/activate
pip install flask numpy gunicorn

### Enable TCP traffic through 5000 port
sudo iptables -I INPUT -p tcp --dport 5000 -j ACCEPT

#### Load the flask app files into this directory

#### Run the app using gunicorn and nginx webserver
gunicorn --bind 0.0.0.0:5000 app:app &
